import { Injectable } from '@angular/core';
import { LoadingController, Platform } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';

@Injectable()
export class UxProvider {
  loading: any;

  constructor(
    private loadingCtrl: LoadingController,
    private platform: Platform,
    private dialogs: Dialogs
  ) {
    this.loading = this.loadingCtrl.create({ content: 'Cargando...' });
  }

  showCargando() {
    if(this.loading) {
      this.loading.present();
    } else {
      this.loading = this.loadingCtrl.create({ content: 'Cargando...' });
      this.loading.present();
    }
  }

  hideCargando() {
    if(this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  alerta(mensaje: string) {
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(this.platform.is('cordova')) {
      this.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }
}