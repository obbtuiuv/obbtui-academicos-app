import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVar } from '../../config';

@Injectable()
export class AsignaturasProvider {
  url: string;
  url_estudiantes: string;
  url_correo: string;

  constructor(private http: HttpClient) {
    this.url = GlobalVar.BASE_API_URL + 'asignaturas';
    this.url_estudiantes = GlobalVar.BASE_API_URL + 'asignaturas/estudiantes';
    this.url_correo = GlobalVar.BASE_API_URL + 'asignaturas/correo';
  }

  getAsignaturas(token: string): Observable<any> {
    var headers = { headers: new HttpHeaders().set('Authorization', token) };
    return this.http
      .post(this.url, null, headers)
      .map(function(response) { return response })
      .catch(this.handleError);
  }

  getEstudiantes(token: string, asignatura: string): Observable<any> {
    var headers = { headers: new HttpHeaders().set('Authorization', token) },
      arg = { asignatura: asignatura };
    return this.http
      .post(this.url_estudiantes, { arg: JSON.stringify(arg) }, headers)
      .map(function(response) { return response })
      .catch(this.handleError);
  }

  sendCorreo(token: string, asignatura: object, asunto: string, mensaje: string, estudiantes: object) {
    var headers = { headers: new HttpHeaders().set('Authorization', token) },
      arg = { asignatura: asignatura, asunto: asunto, mensaje: mensaje, estudiantes: estudiantes };
    return this.http
      .post(this.url_correo, { arg: JSON.stringify(arg) }, headers)
      .map(function(response) { return response })
      .catch(this.handleError);

  }

  private handleError(error: any): Promise<any> {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }
}