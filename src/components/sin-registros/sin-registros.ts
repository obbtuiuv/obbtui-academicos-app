import { Component, Input } from '@angular/core';

@Component({
  selector: 'sin-registros',
  templateUrl: 'sin-registros.html'
})
export class SinRegistrosComponent {
  @Input() mensaje: String;

  constructor() {
  }
}