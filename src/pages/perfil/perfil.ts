import { App, NavController, Platform } from 'ionic-angular';
import { Component } from '@angular/core';
import { Dialogs } from '@ionic-native/dialogs';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NativeStorage } from '@ionic-native/native-storage';
import { AccesoPage } from '../acceso/acceso';
import { AulaVirtualProvider } from '../../providers/aula-virtual/aula-virtual';
import { PortalAcademicoProvider } from '../../providers/portal-academico/portal-academico';
import { PeriodoProvider } from '../../providers/periodo/periodo';
import { UxProvider } from '../../providers/ux/ux';
import { GlobalVar } from '../../config';

@Component({
  selector: 'perfil-page',
  templateUrl: 'perfil.html'
})
export class PerfilPage {
  perfil: any;

  constructor(
    private app: App,
    private dialogs: Dialogs,
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private nav: NavController,
    private platform: Platform,
    private aulaVirtualProvider: AulaVirtualProvider,
    private portalAcademicoProvider: PortalAcademicoProvider,
    private periodoProvider: PeriodoProvider,
    private ux: UxProvider
  ) {
    this.nativeStorage.getItem('fotografia')
      .then(fotografia => {
        this.nativeStorage.getItem('movilidad')
          .then(movilidad => {
            this.nativeStorage.getItem('nombres')
              .then(nombres => {
                this.nativeStorage.getItem('apellidos')
                  .then(apellidos => {
                    this.nativeStorage.getItem('email')
                      .then(email => {
                        this.nativeStorage.getItem('carrera')
                          .then(carrera => {
                            this.nativeStorage.getItem('descEstado')
                              .then(descEstado => {
                                this.perfil = {
                                  fotografia: fotografia,
                                  movilidad: movilidad,
                                  nombres: nombres,
                                  apellidos: apellidos,
                                  email: email,
                                  carrera: carrera,
                                  descEstado: descEstado,
                                  periodo: {
                                    periodo: null,
                                    anio: null
                                  }
                                };
                              }, err => {
                                console.log('Error al obtener \'descEstado\': ', err);
                              });
                          }, err => {
                            console.log('Error al obtener \'carrera\': ', err);
                          });
                      }, err => {
                        console.log('Error al obtener \'email\': ', err);
                      });
                  }, err => {
                    console.log('Error al obtener \'apellidos\': ', err);
                  });
              }, err => {
                console.log('Error al obtener \'nombres\': ', err);
              });
          }, err => {
            console.log('Error al obtener \'movilidad\': ', err);
          });
      }, err => {
        console.log('Error al obtener \'fotografia\': ', err);
      });

    // Verificar sesión del dispositivo
    // this.events.publish('verificarSesion');

    /*
    this.nativeStorage.getItem('token')
      .then(data => {
        this.periodo(data);
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
    */
  }

  ionViewDidLoad() {
    this.ga.trackView('page-perfil');
  }

  portalAcademico() {
    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.portalAcademicoProvider.getUrlPortal(token)
          .subscribe(dataPortal => {
            this.ux.hideCargando();
            console.log(dataPortal);
            if(dataPortal.status == 'ERROR') {
              this.ux.alerta(dataPortal.error.message);
            } else if(dataPortal.status == 'OK') {
              window.open(dataPortal.data.url, '_system');
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  cambiarClave() {
    window.open(GlobalVar.OLVIDAR_CONTRASENA, '_system');
  }

  aulaVirtual() {
    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.aulaVirtualProvider.getUrlAula(token)
          .subscribe(dataAula => {
            this.ux.hideCargando();
            console.log(dataAula);
            if(dataAula.status == 'ERROR') {
              this.ux.alerta(dataAula.error.message);
            } else if(dataAula.status == 'OK') {
              window.open(dataAula.data.url, '_system');
            }
          }, err =>  {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  cerrar() {
    this.nativeStorage.getItem('dispositivo')
      .then(dispositivo => {
        let disp = dispositivo;
        this.nativeStorage.clear()
          .then(() => {
            this.nativeStorage.setItem('dispositivo', disp);
            this.app.getRootNav().setRoot(AccesoPage);
            this.nav.popToRoot();
          }, err => {
            console.log('Error al borrar sesión: ', err);
          });
      }, err => {
        console.log('Error al obtener \'dispositivo\': ', err);
      });
  }

  salir() {
    let titulo = 'Alerta';
    let botones = ['Salir', 'Quedarse']; // Salir = 2, Quedarse = 1

    if(this.platform.is('cordova')) {
      this.dialogs.confirm('¿Estás seguro(a) que deseas salir?', titulo, botones)
        .then(salir => {
          if(salir == 2) {
            this.cerrar();
          }
        })
        .catch(err => {
          console.log('Error al mostrar el diálogo: ', err);
        });
    } else {
      console.error('No se puede mostrar un mensaje de confirmación en navegador.');
      this.cerrar();
    }
  }

  periodo(token: string) {
    this.ux.showCargando();
    this.periodoProvider.getPeriodo(token)
      .subscribe(dataPeriodo => {
        this.ux.hideCargando();
        if(dataPeriodo.status == 'ERROR') {
          this.ux.alerta(dataPeriodo.error.message);
        } else if(dataPeriodo.status == 'OK') {
          this.perfil.periodo.anio = dataPeriodo.data.anio;
          this.perfil.periodo.semestre = dataPeriodo.data.semestre;
        }
      }, err => {
        this.ux.hideCargando();
        this.ux.alerta(GlobalVar.SIN_CONEXION);
      });
  }
}