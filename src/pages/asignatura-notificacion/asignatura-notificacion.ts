import { Component } from '@angular/core';
import { Dialogs } from '@ionic-native/dialogs';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NativeStorage } from '@ionic-native/native-storage';
import { NavParams, Platform } from 'ionic-angular';
import { AsignaturasProvider } from '../../providers/asignaturas/asignaturas';
import { NotificacionesProvider } from '../../providers/notificaciones/notificaciones';
import { UxProvider } from '../../providers/ux/ux';
import { GlobalVar } from '../../config';

@Component({
  selector: 'page-asignatura-notificacion',
  templateUrl: 'asignatura-notificacion.html',
})
export class AsignaturaNotificacionPage {
  asignatura: any;
  notificacion: any;
  tipos: any;
  lista: any;
  estudiantes: any;

  constructor(
    private dialogs: Dialogs,
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private navParams: NavParams,
    private platform: Platform,
    private asignaturasProvider: AsignaturasProvider,
    private notificacionesProvider: NotificacionesProvider,
    private ux: UxProvider
  ) {
    this.asignatura = this.navParams.get('asignatura');
    this.lista = true;
    this.notificacion = {
      tipo: '',
      mensaje: '',
      estudiantes: []
    };

    // Obtener estudiantes
    this.getEstudiantes(this.asignatura);

    // Obtener tipos de notificaciones
    this.getTiposNotificaciones();
  }

  ionViewDidLoad() {
    this.ga.trackView('page-asignatura-notificacion');
  }

  getEstudiantes(asignatura) {
    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.asignaturasProvider.getEstudiantes(token, asignatura)
          .subscribe(dataEstudiantes => {
            this.ux.hideCargando();
            if(dataEstudiantes.status == 'ERROR') {
              this.ux.alerta(dataEstudiantes.error.message);
            } else if(dataEstudiantes.status == 'OK') {
              this.estudiantes = dataEstudiantes.data;
              this.notificacion.estudiantes = this.estudiantes.map(data => data.run);
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  getTiposNotificaciones() {
    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.notificacionesProvider.getTipos(token)
          .subscribe(dataTipos => {
            this.ux.hideCargando();
            if(dataTipos.status == 'ERROR') {
              this.ux.alerta(dataTipos.error.message);
            } else if(dataTipos.status == 'OK') {
              this.tipos = dataTipos.data;
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  sendNotificacion() {
    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.notificacionesProvider.sendNotificacion(token, this.asignatura, this.notificacion.tipo, this.notificacion.mensaje, this.notificacion.estudiantes)
          .subscribe(dataOS => {
            this.ux.hideCargando();
            if(dataOS.status == 'ERROR') {
              this.ux.alerta(dataOS.error.message);
            } else if(dataOS.status == 'OK') {
              this.ux.alerta('Notificación enviada satisfactoriamente');
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  enviarNotificacion() {
    let titulo = 'Alerta';
    let botones = ['Cancelar', 'Enviar']; // Cancelar = 2, Enviar = 1

    if(this.platform.is('cordova')) {
      this.dialogs.confirm('¿Estás seguro(a) que deseas enviar esta notificación?', titulo, botones)
        .then(salir => {
          if(salir == 1) {
            this.sendNotificacion();
          }
        })
        .catch(err => {
          console.log('Error al mostrar el diálogo: ', err);
        });
    } else {
      console.error('No se puede mostrar un mensaje de confirmación en navegador.');
      this.sendNotificacion();
    }
  }
}