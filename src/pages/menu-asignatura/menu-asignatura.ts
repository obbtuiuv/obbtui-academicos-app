import { Component } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NavController, NavParams } from 'ionic-angular';
import { AsignaturaCorreoPage } from '../asignatura-correo/asignatura-correo';
import { AsignaturaNotificacionPage } from '../asignatura-notificacion/asignatura-notificacion';

@Component({
  selector: 'page-menu-asignatura',
  templateUrl: 'menu-asignatura.html',
})
export class MenuAsignaturaPage {
  asignatura: any;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private ga: GoogleAnalytics
  ) {
    this.asignatura = this.navParams.get('asignatura');

    // Verificar sesión del dispositivo
    // this.events.publish('verificarSesion');
  }

  ionViewDidLoad() {
    this.ga.trackView('page-menu-asignatura');
  }

  abrirNotificacion(asignatura) {
    this.navCtrl.push(AsignaturaNotificacionPage, { asignatura: asignatura });
  }

  abrirCorreo(asignatura) {
    this.navCtrl.push(AsignaturaCorreoPage, { asignatura: asignatura });
  }
}