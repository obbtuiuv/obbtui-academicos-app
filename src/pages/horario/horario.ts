import { Component } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NativeStorage } from '@ionic-native/native-storage';
import { HorarioProvider } from '../../providers/horario/horario';
import { UxProvider } from '../../providers/ux/ux';
import { GlobalVar } from '../../config';

@Component({
  selector: 'page-horario',
  templateUrl: 'horario.html',
})
export class HorarioPage {
  diaActual: any;
  diasSemana: any;
  bloques: any;
  horario: any;
  str_diasSemana: any;

  constructor(
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private horarioProvider: HorarioProvider,
    private ux: UxProvider
  ) {
    this.str_diasSemana = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
    this.diasSemana = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    this.horario = {};

    // Verificar sesión del dispositivo
    // this.events.publish('verificarSesion');

    // Definir arreglo con días de la semana
    for(var i = 0; i < this.str_diasSemana.length; i++) {
      this.horario[ this.str_diasSemana[i] ] = [];
    }

    // Definir día actual
    let dia = new Date().getDay();
    switch(dia) {
      case 0:
      case 1:
        this.diaActual = 0;
        break;
      case 2:
        this.diaActual = 1;
        break;
      case 3:
        this.diaActual = 2;
        break;
      case 4:
        this.diaActual = 3;
        break;
      case 5:
        this.diaActual = 4;
        break;
      case 6:
        this.diaActual = 5;
        break;
    }


    this.nativeStorage.getItem('token')
      .then(token => {
        // Buscar bloques de horario
        this.getBloques(token);

        // Buscar el horario semanal
        this.getHorario(token);
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  getBloques(token: string) {
    this.ux.showCargando();
    this.horarioProvider.getBloques(token)
      .subscribe(dataBloques => {
        this.ux.hideCargando();
        if(dataBloques.status == 'ERROR') {
          this.ux.alerta(dataBloques.error.message);
        } else if(dataBloques.status == 'OK') {
          this.bloques = dataBloques.data;
        }
      }, err => {
        this.ux.hideCargando();
        this.ux.alerta(GlobalVar.SIN_CONEXION);
      });
  }

  getHorario(token: string) {
    this.ux.showCargando();
    this.horarioProvider.getHorario(token)
      .subscribe(dataHorario => {
        this.ux.hideCargando();
        if(dataHorario.status == 'ERROR') {
          this.ux.alerta(dataHorario.error.message);
        } else if(dataHorario.status == 'OK') {
          let data = dataHorario.data;
          this.horario['Lunes'] = data.Lunes;
          this.horario['Martes'] = data.Martes;
          this.horario['Miercoles'] = data.Miercoles;
          this.horario['Jueves'] = data.Jueves;
          this.horario['Viernes'] = data.Viernes;
          this.horario['Sabado'] = data.Sabado;
        }
      }, err => {
        this.ux.hideCargando();
        this.ux.alerta(GlobalVar.SIN_CONEXION);
      });
  }

  ionViewDidLoad() {
    this.ga.trackView('page-horario');
  }
}