import { Component } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NativeStorage } from '@ionic-native/native-storage';
import { NovedadesProvider } from '../../providers/novedades/novedades';
import { UxProvider } from '../../providers/ux/ux';
import { GlobalVar } from '../../config';

@Component({
  selector: 'novedades-page',
  templateUrl: 'novedades.html',
})
export class NovedadesPage {
  novedades: any;

  constructor(
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private novedadesProvider: NovedadesProvider,
    private ux: UxProvider
  ) {
    // Verificar sesión del dispositivo
    // this.events.publish('verificarSesion');

    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.novedadesProvider.getNovedades(token)
          .subscribe(dataNovedades => {
            this.ux.hideCargando();
            console.log(dataNovedades);
            if(dataNovedades.status == 'ERROR') {
              this.ux.alerta(dataNovedades.error.message);
            } else if(dataNovedades.status == 'OK') {
              this.novedades = dataNovedades.data;
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  ionViewDidLoad() {
    this.ga.trackView('page-novedades');
  }
}
