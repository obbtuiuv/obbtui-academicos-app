import { Component } from '@angular/core';

import { NovedadesPage } from '../novedades/novedades';
import { PerfilPage } from '../perfil/perfil';
import { NotificacionesPage } from '../notificaciones/notificaciones';

@Component({
  selector: 'tabs-navigation',
  templateUrl: 'tabs-navigation.html'
})
export class TabsNavigationPage {
  tab1Root: any;
  tab2Root: any;
  tab3Root: any;
  id: string;

  constructor() {
    this.tab1Root = NovedadesPage;
    this.tab2Root = PerfilPage;
    this.tab3Root = NotificacionesPage;
  }
}