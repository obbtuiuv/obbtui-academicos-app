import { Component } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
  selector: 'page-credencial',
  templateUrl: 'credencial.html',
})
export class CredencialPage {

  constructor(private ga: GoogleAnalytics) { }

  ionViewDidLoad() {
    this.ga.trackView('page-credencial');
  }
}