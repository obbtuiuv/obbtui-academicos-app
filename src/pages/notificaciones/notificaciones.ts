import { Component } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NativeStorage } from '@ionic-native/native-storage';
import { NotificacionesProvider } from '../../providers/notificaciones/notificaciones';
import { UxProvider } from '../../providers/ux/ux';
import { GlobalVar } from '../../config';

@Component({
  selector: 'notificaciones-page',
  templateUrl: 'notificaciones.html'
})
export class NotificacionesPage {
  notificaciones: any;

  constructor(
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private notificacionesProvider: NotificacionesProvider,
    private ux: UxProvider
  ) {
  }

  ionViewDidLoad() {
    this.ga.trackView('page-notificaciones');
  }

  ionViewDidEnter() {
    // Verificar sesión del dispositivo
    // this.events.publish('verificarSesion');
    this.cargarNotificaciones();
  }

  cargarNotificaciones() {
    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.notificacionesProvider.getNotificaciones(token)
          .subscribe(dataNotificaciones => {
            this.ux.hideCargando();
            console.log(dataNotificaciones);
            if(dataNotificaciones.status == 'ERROR') {
              this.ux.alerta(dataNotificaciones.error.message);
            } else if(dataNotificaciones.status == 'OK') {
              this.notificaciones = dataNotificaciones.data;
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }
}