import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage';
import { NavController } from 'ionic-angular';
import { PresentacionPage } from '../presentacion/presentacion';

@Component({
  selector: 'settings-page',
  templateUrl: 'configuracion.html'
})
export class ConfiguracionPage {
  settingsForm: FormGroup;
  // make PresentacionPage the root (or first) page
  rootPage: any = PresentacionPage;
  perfil: any;

  constructor(
    private nativeStorage: NativeStorage,
    private nav: NavController
  ) {
    this.settingsForm = new FormGroup({
      nombre: new FormControl(),
      pais: new FormControl(),
      cargo: new FormControl(),
      empresa: new FormControl(),
      biografia: new FormControl(),
      notificaciones: new FormControl()
    });
  }

  ionViewDidLoad() {

  }

  logout() {
    // navigate to the new page if it is not the current page
    this.nativeStorage.clear()
      .then(() => {
        this.nav.push(this.rootPage);
      }, err => {
        console.log('Error al borrar sesión: ', err);
      });
  }
}
