import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, App, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NotificacionesProvider } from '../providers/notificaciones/notificaciones';
import { SesionProvider } from '../providers/sesion/sesion';
import { UxProvider } from '../providers/ux/ux';
import { AccesoPage } from '../pages/acceso/acceso';
import { AsignaturasPage } from '../pages/asignaturas/asignaturas';
import { CasinosPage } from '../pages/casinos/casinos';
import { CredencialPage } from '../pages/credencial/credencial';
import { DeudasPage } from '../pages/deudas/deudas';
import { EnlacesPage } from '../pages/enlaces/enlaces';
import { EventosPage } from '../pages/eventos/eventos';
import { HorarioPage } from '../pages/horario/horario';
import { InformacionPage } from '../pages/informacion/informacion';
import { PresentacionPage } from '../pages/presentacion/presentacion';
import { PuntosPage } from '../pages/puntos/puntos';
import { SalirPage } from '../pages/configuracion/salir';
import { SantanderPage } from '../pages/configuracion/santander';
import { TabsNavigationPage } from '../pages/tabs-navigation/tabs-navigation';
import { GlobalVar } from '../config';
import { NativeStorage } from '@ionic-native/native-storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  // rootPage: any = localStorage.getItem('token') ? TabsNavigationPage :
  //  localStorage.getItem('presentacion') == null ? PresentacionPage : AccesoPage;
  pages: Array<{ title: string, icon: string, component: any }>;
  pushPages: Array<{ title: string, icon: string, component: any }>;

  constructor(
    private platform: Platform,
    private menu: MenuController,
    private app: App,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private events: Events,
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private notificacionesProvider: NotificacionesProvider,
    private sesionProvider: SesionProvider,
    private ux: UxProvider
  ) {
    this.platform.ready()
      .then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        this.nativeStorage.getItem('token')
          .then(data => {
            this.rootPage = TabsNavigationPage;
          }, err => {
            this.nativeStorage.getItem('presentacion')
              .then(data => {
                this.rootPage = AccesoPage;
              }, err => {
                this.rootPage = PresentacionPage;
              });
          });

        this.events.subscribe('verificarSesion', () => {
          this.nativeStorage.getItem('token')
            .then(data => {
              var token = data;
              this.verificarSesion(token);
            }, err => {
              console.log('Error al obtener \'token\': ', err);
            });
          //let token = localStorage.getItem('token') || 'sinToken';
        });

        this.splashScreen.hide();
        this.statusBar.styleDefault();

        if(!this.platform.is('cordova')) {
          console.log('Google Analytics no funciona en navegadores.');
        } else {
          // Cargar Google Analytics
          this.ga.startTrackerWithId(GlobalVar.GANALYTICS_API_KEY)
            .then(() => {
              console.log('Google Analytics está listo.');
            })
            .catch((e) => {
              console.log('Error al cargar Google Analytics', e);
            });
        }

        // Cargar notificaciones push
        this.notificacionesProvider.startNotificacionesPush();
    });

    // Verificar sesión del dispositivo si existe
    /*
    if(localStorage.getItem('token') != null) {
      // this.verificarSesion(token);
    }
    */

    this.pages = [
      { title: 'Inicio', icon: 'home', component: TabsNavigationPage },
      { title: 'Mi TUI', icon: 'card', component: CredencialPage },
      { title: 'Casinos', icon: 'restaurant', component: CasinosPage },
      { title: 'Eventos', icon: 'calendar', component: EventosPage },
      { title: 'Asignaturas', icon: 'book', component: AsignaturasPage },
      { title: 'Horario', icon: 'time', component: HorarioPage },
      { title: 'Enlaces', icon: 'link', component: EnlacesPage },
      { title: 'Puntos de interés', icon: 'pin', component: PuntosPage },
      // { title: 'Saldos', icon: 'cash', component: DeudasPage },
      { title: 'Banco Santander', icon: 'phone-portrait', component: SantanderPage },
      { title: 'Información', icon: 'information-circle', component: InformacionPage },
      { title: 'Salir', icon: 'log-out', component: SalirPage }
    ];

    this.pushPages = [];
  }

  openPage(page) {
    this.menu.close();
    this.nav.setRoot(page.component);
  }

  pushPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // rootNav is now deprecated (since beta 11) (https://forum.ionicframework.com/t/cant-access-rootnav-after-upgrade-to-beta-11/59889)
    this.app.getRootNav().push(page.component);
  }

  verificarSesion(token: string) {
    this.sesionProvider.verificar(token)
      .subscribe(dataSesion => {
        console.log(dataSesion);
        if(dataSesion.hasOwnProperty('data')) {
          if(!dataSesion.data.valida) {
            // Limpiar sesión y mover a la página de acceso
            console.log('Sesión no válida.');
            this.nativeStorage.clear()
              .then(() => {
                this.nativeStorage.setItem('presentacion', 'false')
                  .then(data => {
                    this.nav.setRoot(AccesoPage);
                    this.nav.popToRoot();
                  }, err => {
                    console.log('Error al guardar \'presentacion\': ', err);
                  });
              }, err => {
                console.log('Error al borrar sesión: ', err);
              });
          } else {
            console.log('Sesión válida.');
          }
        } else {
          this.ux.alerta(dataSesion.error.message);
        }
      }, err => {
        this.ux.alerta(GlobalVar.SIN_CONEXION);
      });
  }
}